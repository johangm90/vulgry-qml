/*
 * Copyright (C) 2020  Test
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Test is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VULGRY_H
#define VULGRY_H

#include <QObject>

class Vulgry: public QObject {
    Q_OBJECT

public:
    Vulgry();
    ~Vulgry() = default;

    Q_INVOKABLE void speak();
};

#endif
