import QtQuick 2.7
import Ubuntu.Components 1.3
import "../logic/DateUtils.js" as DateUtils
import "../logic/Api.js" as Api

Page{
    id: searchPage
    visible: false

    Component.onCompleted: {
        vgModel.clear()
        squery.text=""
        squery.forceActiveFocus()
    }

    header: PageHeader {
        id: pageHeader
        title: "Vulgry"

        contents: TextField {
            id: squery
            placeholderText: i18n.tr("Search...")
            anchors.fill: parent
            anchors.rightMargin: units.gu(2)
            anchors.topMargin: units.gu(1.5)
            anchors.bottomMargin: units.gu(1.5)
            Keys.onReturnPressed: {
                if(squery.text!=''){
                    Api.getVG(squery.text);
                    squery.focus=false
                }else{
                    console.log('error')
                }
            }
        }
    }

    Rectangle {
        color: "transparent"
        anchors.top: pageHeader.bottom
        anchors.bottom: searchPage.bottom
        width: parent.width

        ActivityIndicator {
            id: sloader
            anchors.centerIn: parent
        }
        Column{
            id: scontainer
            spacing: units.gu(1)
            anchors {
                fill: parent
            }

            ListModel {
                id: vgModel
            }

            Item {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                ListView {
                    id: vgList
                    clip: true
                    model: vgModel
                    width: scontainer.width
                    height: parent.height
                    boundsBehavior: Flickable.StopAtBounds
                    delegate: ListItem {
                        height: units.gu(8)
                        width: parent.width

                        Row {
                            height: parent.height
                            width: parent.width
                            padding: units.gu(2)
                            spacing: units.gu(2)

                            Item {
                                id: itemImage
                                width: units.gu(10)
                                height: units.gu(6)
                                anchors.verticalCenter: parent.verticalCenter
                                Image {
                                    id: image
                                    source: simg
                                    width: parent.width
                                    height: parent.height
                                    anchors.verticalCenter: parent.verticalCenter
                                    fillMode: Image.PreserveAspectFit
                                }
                            }
                            Column {
                                anchors.verticalCenter: itemImage.verticalCenter
                                width: parent.width - itemImage.width - parent.spacing - parent.padding
                                Label { text: stitle; width: parent.width; elide: Text.ElideRight; wrapMode: Text.Wrap; maximumLineCount: 1;}
                                Label { text: schannel; width: parent.width; elide: Text.ElideRight; wrapMode: Text.Wrap; maximumLineCount:1; textSize: Label.Small; color: "#898B8C"; }
                                Label { text: DateUtils.timeSince(sdate); width: parent.width; textSize: Label.Small; color: "#898B8C"; }
                            }
                        }

                        onClicked: {
                            /*for (var prop in videoPage) {
                                print(prop += " (" + typeof(videoPage[prop]) + ") = " + videoPage[prop]);
                            }*/
                            videoPage.getDownloadLinks(surl)
                            pageStack.push(videoPage)
                        }
                    }
                }
                Scrollbar {
                    flickableItem: vgList
                    align: Qt.AlignTrailing
                }
            }
        }
    }
}
