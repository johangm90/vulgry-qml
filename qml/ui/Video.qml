import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.0 as UListItem
import Ubuntu.Components.Popups 1.3
import Ubuntu.DownloadManager 1.2
import Vulgry 1.0
import "../logic/DateUtils.js" as DateUtils
import "../logic/Api.js" as Api
import "../components"

Page {
    id: videoPage
    visible: false

    function getDownloadLinks(url) {
        Api.getLinks(url)
    }

    header: PageHeader {
        id: pageHeader
        title: "Vulgry"
    }

    Rectangle {
        color: "transparent"
        anchors.top: pageHeader.bottom
        anchors.bottom: videoPage.bottom
        width: parent.width

        ActivityIndicator {
            id: activity
            anchors.centerIn: parent
        }
        Column{
            id: jgm
            spacing: units.gu(1)
            anchors {
                topMargin: units.gu(2)
                fill: parent
            }

            UbuntuShape {
                id: thumbnailShape
                width: units.gu(32)
                height: units.gu(18)
                anchors.horizontalCenter: parent.horizontalCenter
                image: Image {
                    id: thumbnailImage
                    fillMode: Image.PreserveAspectFit
                    source: ""
                }
                visible: false
            }

            UListItem.ThinDivider {
                id: divider1
                visible: false
                anchors.top: thumbnailShape.botton
                anchors.topMargin: units.gu(2)
            }

            ListModel {
                id: urlModel
            }

            Item {
                anchors.top: divider1.bottom
                anchors.bottom: parent.bottom
                ListView {
                    id: urlView
                    clip: true
                    model: urlModel
                    width: jgm.width
                    height: parent.height
                    boundsBehavior: Flickable.StopAtBounds
                    delegate: ListItem {
                        property variant colors: ["#24c6d5", "#57dd86", "#ad7dcf", "#ff484d", "#fcba59"]
                        Component.onCompleted: {
                            var color = index % colors.length
                            rformat.color=colors[color]
                        }
                        contentItem.anchors {
                            leftMargin: units.gu(2)
                            rightMargin: units.gu(2)
                            topMargin: units.gu(0.5)
                            bottomMargin: units.gu(0.5)
                        }
                        Rectangle {
                            id: rformat
                            color: "#24c6d5"
                            width: height
                            height: parent.height
                            radius: height/2
                            anchors.verticalCenter: parent.verticalCenter;
                            Label {
                                text: format
                                color: "#fff"
                                fontSize: "small"
                                anchors.centerIn: parent
                            }
                        }
                        Rectangle {
                            height: parent.height
                            anchors.left: rformat.right;
                            anchors.leftMargin: units.gu(1.6);
                            Label {
                                text: size
                                anchors.verticalCenter: parent.verticalCenter
                            }
                        }

                        onClicked: {
                            downloader.name = videoPage.title
                            downloader.ext = format
                            PopupUtils.open(dialogComponent)
                            downloader.download(link)
                        }
                    }
                }
                Scrollbar {
                    flickableItem: urlView
                    align: Qt.AlignTrailing
                }
            }
        }
    }

    FileManager {
        id: fileManager
    }

    SingleDownload {
        id: downloader

        property string name
        property string ext

        metadata: {
            showInIndicator: true
            title: videoPage.title
        }
    }

    Component {
        id: exportPage
        ExportPage { }
    }

    Component {
        id: dialogComponent

        Dialog {
            id: dialog
            title: i18n.tr("Downloading")
            text: videoPage.title

            ProgressBar {
                minimumValue: 0
                maximumValue: 100
                value: downloader.progress
            }

            Connections {
                target: downloader
                onFinished: {
                    PopupUtils.close(dialog)
                    var fileName = downloader.name + '.' + downloader.ext
                    var downloadPath = fileManager.saveDownload(path, fileName)

                    var exporter = exportPage.createObject(vulgry);
                    exporter.downloadId = downloader.downloadId
                    exporter.path = downloadPath
                    pageStack.push(exporter)
                }
            }

            Button {
                text: i18n.tr("Cancel")
                color: UbuntuColors.red
                onClicked: {
                    downloader.cancel()
                    PopupUtils.close(dialog)
                }
            }
        }
    }
}
