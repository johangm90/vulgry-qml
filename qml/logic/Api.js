function getLinks(videoId){
    urlModel.clear();
    thumbnailShape.visible = false;
    divider1.visible = false;
    videoPage.title = 'Vulgry';
    activity.running=true
    var xhr = new XMLHttpRequest;
    var url = 'http://www.vulgry.com/api.php?url=' + videoId;

    xhr.open("GET", url);

    xhr.onreadystatechange = function() {
        if (xhr.readyState == XMLHttpRequest.DONE) {
            var data = xhr.responseText;
            //console.log(data);
            data = eval(data);
            var title=data.title;
            var thumbnail=data.thumbnail;
            videoPage.title = title;
            thumbnailShape.visible = true;
            divider1.visible = true;
            thumbnailImage.source = thumbnail;
            var vglist =[];
            if("formats" in data){
                for ( var idx1 = 0; idx1 < data.formats.length; idx1++ ) {
                    //if(!("format_note" in data.formats[idx1])){
                    if("format_note" in data.formats[idx1]){
                        if(data.formats[idx1].width && data.formats[idx1].height){
                            var size = data.formats[idx1].width + 'x' + data.formats[idx1].height
                        }else{
                            size = 'unknown';
                        }
                        var link = data.formats[idx1].url + '&title=' + encodeURIComponent(title);
                        var format = data.formats[idx1].ext
                        vglist.splice(0, 0, {url: link, ext: format, size: size});
                    }
                }
                for ( var i = 0; i < vglist.length; i++ ) {
                    urlModel.append({link: vglist[i].url, format: vglist[i].ext, size: vglist[i].size});
                }
            }else{
                if(data.width && data.height){
                    size = data.width + 'x' + data.height
                }else{
                    size = 'unknown';
                }
                link = data.url + '&title=' + encodeURIComponent(title);
                urlModel.append({link: link, format: data.ext, size: size});
            }
            activity.running=false
        }
    }
    xhr.send();
}

function getVG(q){
    vgModel.clear();
    searchPage.title = 'Vulgry';
    sloader.running = true;

    var xhr = new XMLHttpRequest;
    xhr.open("GET", 'http://www.vulgry.com/test.php?q='+q+'&maxResults=50');
    xhr.onreadystatechange = function() {
        if (xhr.readyState == XMLHttpRequest.DONE) {
            var data = xhr.responseText;
            data = eval(data);
            for ( var i = 0; i < data.length; i++ ) {
                vgModel.append({
                  simg: data[i].thumbnail_medium,
                  stitle: data[i].title,
                  schannel: data[i].channel_title,
                  sdate: data[i].published_at,
                  surl: "https://www.youtube.com/watch?v="+data[i].id
                });
            }
            sloader.running=false
        }
    }
    xhr.send();
}
