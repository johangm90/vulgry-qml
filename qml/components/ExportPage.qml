import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Content 1.3

Page {
    id: picker

    property var activeTransfer
    property var selectedItems
    property var downloadId
    property var path

    header: PageHeader {
        title: i18n.tr("Save with")
    }

    ContentPeerPicker {
        anchors { fill: parent; topMargin: picker.header.height }
        visible: parent.visible
        showTitle: false
        contentType: ContentType.All
        handler: ContentHandler.Destination

        onPeerSelected: {
            activeTransfer = peer.request()
            activeTransfer.downloadId = downloadDialog.downloadId
            activeTransfer.state = ContentTransfer.Downloading
            stateChangeConnection.target = activeTransfer
        }

        onCancelPressed: {
            pageStack.pop()
        }
    }

    Connections {
        id: stateChangeConnection
        onStateChanged: {
            if (activeTransfer.state === ContentTransfer.InProgress) {
                var contentItems = [];
                contentItems.push(transferComponent.createObject(vulgry, {"url": path}))
                activeTransfer.items = contentItems;
                activeTransfer.state = ContentTransfer.Charged;

                pageStack.pop()
            }
        }
    }
}
