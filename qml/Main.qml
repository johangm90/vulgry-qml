/*
 * Copyright (C) 2020  Johan Guerreros
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * vulgry is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Ubuntu.Content 1.3
import Ubuntu.DownloadManager 1.2
import Ubuntu.Components.Popups 1.3
import Qt.labs.settings 1.0
import "logic/Api.js" as Api
import "components"
import "ui"

MainView {
    id: vulgry

    objectName: 'mainView'
    applicationName: 'vulgry.johangm90'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    property list<ContentItem> transferItemList
    property var contentTransfer
    property string app_version: "1.2.0"

    property var settings: Settings {
        property string primaryColor: "#0169c9"
        property string theme: "Ambiance"
        onThemeChanged: Theme.name = "Ubuntu.Components.Themes." + settings.theme
    }

    Component {
        id: transferComponent
        ContentItem { }
    }

    Component {
        id: searchPage
        Search {}
    }

    Video {
        id: videoPage
        visible: false
    }

    Component {
        id: aboutPage
        About {}
    }

    Component {
        id: downloadDialog
        DownloadDialog { }
    }

    PageStack{
        id: pageStack

        Component.onCompleted: push(mainPage)

        Page {
            id: mainPage
            visible: false

            header: PageHeader {
                id: mainHeader
                title: "Vulgry"

                trailingActionBar.actions: [
                    Action {
                        text: i18n.tr("Search")
                        iconName: "search"
                        onTriggered: pageStack.push(searchPage)
                    },
                    Action {
                        text: i18n.tr("Theme")
                        iconName: (vulgry.settings.theme == "Ambiance") ? "torch-on" : "torch-off"
                        onTriggered: vulgry.settings.theme = (vulgry.settings.theme == "Ambiance") ? "SuruDark" : "Ambiance"
                    },
                    Action {
                        text: i18n.tr("Donate")
                        iconName: "like"
                        onTriggered: Qt.openUrlExternally("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=8YW88CE8YLYZ2")
                    },
                    Action {
                        text: i18n.tr("About")
                        iconName: "help"
                        onTriggered: pageStack.push(aboutPage)
                    }
                ]
            }

            Rectangle {
                anchors.top: mainHeader.bottom
                anchors.bottom: mainPage.bottom
                width: parent.width
                color: "transparent"

                Rectangle {
                    anchors.fill: parent
                    anchors.margins: units.gu(2)
                    color: "transparent"

                    TextField {
                        id: txtUrl
                        placeholderText: i18n.tr("Video URL")
                        width: parent.width
                    }
                }
            }

            Rectangle {
                id: process
                color: Qt.lighter("#0169c9", 2);
                width: units.gu(20)
                height: units.gu(20)
                radius: units.gu(10)
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter

                Rectangle {
                    color: "#0169c9"
                    width: units.gu(18)
                    height: units.gu(18)
                    radius: units.gu(9)
                    anchors.centerIn: parent

                    Text{
                        id: go
                        text: i18n.tr("DOWNLOAD")
                        color: "#fff"
                        anchors.verticalCenter: parent.verticalCenter
                        anchors {
                            left: parent.left
                            right: parent.right
                        }
                        font.pointSize: units.gu(2)
                        wrapMode: Text.Wrap
                        horizontalAlignment:Text.AlignHCenter
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if (txtUrl.text != '') {
                                videoPage.getDownloadLinks(txtUrl.text)
                                pageStack.push(videoPage)
                            }
                        }
                    }
                }
            }
        }
    }

    Component {
        id: downloadComponent
        SingleDownload {
            autoStart: false
            property var contentType
            property string name

            metadata: Metadata {
                showInIndicator: true
                title: name
            }

            onDownloadIdChanged: {
                console.log(downloadId)
                PopupUtils.open(downloadDialog, vulgry, {"contentType" : ContentType.All, "downloadId" : downloadId})
            }

            onFinished: {
                destroy()
            }
        }
    }
}
